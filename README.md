---
title: MacOS Laptop Setup
---

---
**Warning** This repository is deprecated. See my [dotfiles](https://gitlab.com/pirivan/dotfiles) repo instead
---

# macos laptop setup

A repo to host config files and notes about the setup of my Mac Pro laptop

* Install `oh-my-zsh`:
  ```sh
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  ```

* Clone this repository

 
## Homebrew packages

The following are packages I installed explicitly using `homebrew`:


| Package                                                          | Comment                                                |
|------------------------------------------------------------------|--------------------------------------------------------|
| [bat](https://github.com/sharkdp/bat)                            | A supercharged `cat`                                   |
| [duf](https://github.com/muesli/duf)                             | Disk Usage/Free Utility                                |
| [emacs](https://www.gnu.org/software/emacs/)                     | The real text editor (see section below)               |
| [exa](https://the.exa.website/)                                  | Alternative to `ls`                                    |
| [fd](https://github.com/sharkdp/fd)                              | Fast alternative to `find`                             |
| [google-cloud-sdk](https://cloud.google.com/sdk/docs/quickstart) | gcloud commands                                        |
| [gh](https://cli.github.com/)                                    | The GitHub CLI                                         |
| [git](https://git-scm.com/)                                      | The latest git (apple's is outdated)                   |
| [git-delta](https://github.com/dandavison/delta)                 | A syntax-highlighting pager for git, diff, and grep    |
| [glances](https://github.com/nicolargo/glances)                  | A laptop monitoring tool                               |
| [glow](https://github.com/charmbracelet/glow)                    | Render markdown on the CLI                             |
| [gping](https://github.com/orf/gping)                            | A pingcommand replacement                              |
| [htop](https://htop.dev/)                                        | Interactive process viewer                             |
| [jump](https://github.com/gsamokovarov/jump)                     | Quick dir navigator                                    |
| [kitty](https://github.com/kovidgoyal/kitty)                     | A lightweight terminal emulator                        |
| [lazygit](https://github.com/jesseduffield/lazygit)              | A simple terminal UI for git commands (1)              |
| [micro](https://micro-editor.github.io/)                         | Because I don't like `vi`! (2)                         |
| [most](https://www.jedsoft.org/most/index.html)                  | Fancy pager                                            |
| [nmap](https://nmap.org/)                                        | A utility for network discovery and security auditing  |
| [nnn](https://github.com/jarun/nnn)                              | A terminal file manager (3)                            |
| [pandoc](https://pandoc.org/)                                    | A file converter                                       |
| [pyenv](https://github.com/pyenv/pyenv)                          | A Python version management system (see section below) |
| [rg](https://github.com/BurntSushi/ripgrep)                      | A replacement for `grep`                               |
| [tmux](https://github.com/tmux/tmux)                             | **The** terminal multiplexer                           |


(1) install with
`brew install jesseduffield/lazygit/lazygit`

(2) install the Go plugin with
`micro -plugin install go`
  
(3) install plugins with 
`curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs | sh`


* Run the following command to install the packages:
  ```
  brew install bat delta duf exa fd google-cloud-sdk gh git glow htop jump \
  kitty micro most nnn unzip pandoc pyenv rg tmux
  ```

* Change dir to this repo's clone and run the script `./config.sh` to set up all the required config files

* Log out from all terminals and open a new one (try `kitty`!)

## Installing Emacs

Install Emacs using homebrew for use on the terminal only.

``` sh
brew install emacs
brew link emacs
```

### Install Emacs Prelude

Install [Emacs Prelude](https://prelude.emacsredux.com/) which
provides a sensible and well documented setup on top of a default
Emacs installation.

As suggested in the documentation, I use my own
[fork](https://gitlab.com/pirivan/prelude) of the upstream repo to
manage my customizations. More exactly, I mirror the upstream repo
using the
[mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/)
facilities of GitLab.

``` sh
cd ~
rm -rf .emacs .emacs.d
git clone git://gitlab.com/pirivan/prelude.git ~/src/gitlab.com/pirivan/prelude
ln -s ~/src/gitlab.com/pirivan/prelude ~/.emacs.d
```

I keep my changes in the `local` branch.

```sh
cd ~/src/gitlab.com/pirivan/prelude
git checkout -b local
# make commits
git push
```

### Syncyng Upstream Prelude

GitLab automatically syncs the upstream repo. All I have to do is
merge the changes from the `master` branch into the `local` branch.

```sh
cd ~/src/gitlab.com/pirivan/prelude
git checkout master
git pull
git checkout local
git rebase master
git push
```

I keep my customiztions restricted to the `personal` directory. This
ensures that there are no conflicts with the upstream code.


### The Emacs Daemon

I run Emacs as a daemon and use `emasclient` as the editor for faster
startup times. To run Emacs as a daemon upon login:

``` sh
ln -s /usr/local/Cellar/emacs/28.1/homebrew.mxcl.emacs.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.emacs.plist
```

I defined the `ec` alias in `.zshrc` to start the client in a terminal window.

**Note**: Changes to the configuration won't take effect unless you
restart the Emacs daemon:

``` sh
emacsclient -e '(kill-emacs)'
```

MacOS restarts the daemon automatically.


## Installing Python

* Don't touch the system Python installation that comes with MacOS. It is outdated (Python 2.7 and 3.8.9) but
  MacOS may need them.

* Use `Pyenv` instead to manage Python versions:
  ```sh
  brew install pyenv
  pyenv install --list
  pyenv install 3.10.1
  pyenv global 3.10.1
  pyenv version
  ```

  To fix the version of Python in a working directory:
  ```sh
  cd working_dir
  pyenv local 3.9.1
  ```

## Installing LaTeX

Reference website is [MacTeX](https://www.tug.org/mactex/) which install several
[components](https://www.tug.org/mactex/What_Is_Installed.pdf).

* Run the following command:
  ```sh
  brew install --cask mactex
  ```
  The installation takes a quite some time, just be patient

* Reinitialize the terminal window or type `eval "$(/usr/libexec/path_helper)"`

* Install TeXstudio with `brew install texstudio`
